//
//  ViewController.swift
//  DoubleTwitch
//
//  Created by Eric Betts on 6/3/14.
//  Copyright (c) 2014 Eric Betts. All rights reserved.
//

import UIKit

class ViewController: UIViewController, DRDoubleDelegate {

    //DRDriveDirection.Forward // DRDriveDirection.Backward // DRDriveDirection.Stop
    var drive:DRDriveDirection = .Stop

    //[1.0, -1.0, 0.0]// L / R / Straight
    var turn:Float = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        DRDouble.sharedDouble().delegate = self;
        println("SDK Version \(kDoubleBasicSDKVersion)")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//https://github.com/doublerobotics/Basic-Control-SDK-iOS
extension ViewController {

    func doubleDidConnect(theDouble:DRDouble) {
        println("Double connected")
        //Connected
    }
    
    func doubleDidDisconnect(theDouble:DRDouble) {
        println("Double disconnected")
        //Disconnected
    }
    
    func doubleStatusDidUpdate(theDouble:DRDouble) {
        var d = DRDouble.sharedDouble()
        println("Double: \(d.description) \(d.poleHeightPercent) \(d.kickstandState) \(d.batteryPercent) \(d.batteryIsFullyCharged) \(d.firmwareVersion)")
    }
    
    func doubleDriveShouldUpdate(theDouble:DRDouble) {
        theDouble.drive(drive, turn:turn)
    }

}
